package ru.t1consulting.vmironova.tm.repository.model;

import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.api.repository.model.ISessionRepository;
import ru.t1consulting.vmironova.tm.model.Session;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class SessionRepository extends AbstractUserOwnedRepository<Session> implements ISessionRepository {
}
