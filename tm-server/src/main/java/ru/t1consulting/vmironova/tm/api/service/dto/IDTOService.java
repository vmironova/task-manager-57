package ru.t1consulting.vmironova.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.api.repository.dto.IDTORepository;
import ru.t1consulting.vmironova.tm.dto.model.AbstractModelDTO;
import ru.t1consulting.vmironova.tm.enumerated.Sort;

import java.util.List;

public interface IDTOService<M extends AbstractModelDTO> extends IDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable Sort sort) throws Exception;

    void removeById(@Nullable String id) throws Exception;

}
